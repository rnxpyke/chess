#[derive(Copy, Clone, Debug, PartialEq, Hash)]
pub enum Type {
    King,
    Queen,
    Bishop,
    Knight,
    Rook,
    Pawn,
}

#[derive(Copy, Clone, Debug, PartialEq, Hash)]
pub enum Color {
    Black,
    White,
}

impl Color {
    pub fn either<A>(&self, white: A, black: A) -> A {
        use Color::*;
        match self {
            White => white,
            Black => black,
        }
    }
}

#[derive(Clone, Copy, Debug, Hash)]
pub struct Piece {
    pub typ: Type,
    pub color: Color,
}

impl Piece {
    pub const fn new(typ: Type, color: Color) -> Self {
        Self { typ, color }
    }
    pub const fn typ(&self) -> Type {
        self.typ
    }
    pub const fn color(&self) -> Color {
        self.color
    }

    pub fn ascii(&self) -> &'static str {
        use Type::*;
        match self.typ() {
            King => self.color().either("K", "k"),
            Queen => self.color().either("Q", "q"),
            Bishop => self.color().either("B", "b"),
            Knight => self.color().either("N", "n"),
            Rook => self.color().either("R", "r"),
            Pawn => self.color().either("P", "p"),
        }
    }

    pub fn unicode(&self) -> &'static str {
        use Type::*;
        match self.typ() {
            King => self.color().either("♔", "♚"),
            Queen => self.color().either("♕", "♛"),
            Bishop => self.color().either("♗", "♝"),
            Knight => self.color().either("♘", "♞"),
            Rook => self.color().either("♖", "♜"),
            Pawn => self.color().either("♙", "♟︎"),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Move {
    pub from: BoardIdx,
    pub to: BoardIdx,
}

pub type Position = Option<Piece>;

type Row = u8;
type Column = u8;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct BoardIdx {
    pub idx: u8,
}

impl BoardIdx {
    pub const fn new(row: Row, column: Column) -> Self {
        Self {
            idx: (column as u8) ^ (row as u8) << 3,
        }
    }

    pub const fn row(&self) -> Row {
        self.idx >> 3
    }

    pub const fn column(&self) -> Column {
        self.idx & 0b000111
    }

    pub const fn color(&self) -> Color {
        use Color::*;
        if (self.row() + self.column()) % 2 == 0 {
            White
        } else {
            Black
        }
    }
}

impl std::convert::TryFrom<(i8, i8)> for BoardIdx {
    type Error = &'static str;
    fn try_from(value: (i8, i8)) -> Result<Self, Self::Error> {
        let (row, column) = value;
        if (0..8).contains(&row) && (0..8).contains(&column) {
            Ok(BoardIdx::new(row as u8, column as u8))
        } else {
            Err("out of range")
        }
    }
}

use core::ops::{Index, IndexMut};
impl<A> Index<BoardIdx> for Board<A> {
    type Output = A;
    fn index(&self, index: BoardIdx) -> &Self::Output {
        &self.state[index.row() as usize][index.column() as usize]
    }
}

impl<A> IndexMut<BoardIdx> for Board<A> {
    fn index_mut(&mut self, index: BoardIdx) -> &mut Self::Output {
        &mut self.state[index.row() as usize][index.column() as usize]
    }
}

#[derive(Default, Copy, Clone, Hash)]
pub struct Board<P> {
    pub state: [[P; 8]; 8],
}

impl<A> Board<A> {
    pub fn map<B, F: FnMut(A) -> B>(self, mut f: F) -> Board<B> {
        fn map_array_8<A, B, F: FnMut(A) -> B>(a: [A; 8], mut f: F) -> [B; 8] {
            let [a0, a1, a2, a3, a4, a5, a6, a7] = a;
            [f(a0), f(a1), f(a2), f(a3), f(a4), f(a5), f(a6), f(a7)]
        }
        Board {
            state: map_array_8(self.state, |row| map_array_8(row, |p| f(p))),
        }
    }

    pub fn each(&self) -> impl Iterator<Item = (BoardIdx, &A)> {
        self.state.iter().enumerate().flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .map(move |(x, p)| (BoardIdx::new(y as u8, x as u8), p))
        })
    }
}

impl Board<&str> {
    fn view(&self, color: Color) -> String {
        let mut res = String::with_capacity(9 * 8);
        use Color::*;
        let rowiter: Box<dyn Iterator<Item = &[&str; 8]>> = match color {
            White => Box::new(self.state.iter().rev()),
            Black => Box::new(self.state.iter()),
        };
        for row in rowiter {
            let positer: Box<dyn Iterator<Item = &&str>> = match color {
                White => Box::new(row.iter()),
                Black => Box::new(row.iter().rev()),
            };
            for pos in positer {
                res.push_str(pos);
            }
            res.push_str("\n");
        }
        res
    }
}

impl Board<Position> {
    pub fn ascii(&self) -> Board<&str> {
        self.map(|pos| pos.map_or(" ", |p| p.ascii()))
    }

    pub fn unicode(&self) -> Board<&str> {
        self.map(|pos| pos.map_or(" ", |p| p.unicode()))
    }
}

#[derive(Copy, Clone, Hash)]
pub struct Both<A> {
    pub white: A,
    pub black: A,
}

impl<A> Both<A> {
    pub fn new(white: A, black: A) -> Both<A> {
        Both {white, black}
    }

    pub fn choose(&self, c: Color) -> &A {
        match c {
            Color::White => &self.white,
            Color::Black => &self.black,
        }
    }
}

impl<A: Clone> Both<A> {
    pub fn same(a: A) -> Both<A> {
        Both::new(a.clone(),a) 
    }
}

#[derive(Copy, Clone, Hash)]
pub struct Castling {
    pub king: bool,
    pub queen: bool,
}

#[derive(Clone, Hash)]
pub struct ChessState {
    pub board: Board<Position>,
    pub player: Color,
    pub castling: Both<Castling>,
    pub en_passant: Option<BoardIdx>,
    pub halfturns: u32,
}

const fn initial_board() -> Board<Position> {
    use Color::*;
    use Type::*;
    const fn p(typ: Type, color: Color) -> Option<Piece> {
        Some(Piece::new(typ, color))
    }
    const fn officiers(c: Color) -> [Position; 8] {
        [
            p(Rook, c),
            p(Knight, c),
            p(Bishop, c),
            p(Queen, c),
            p(King, c),
            p(Bishop, c),
            p(Knight, c),
            p(Rook, c),
        ]
    };
    const fn pawns(c: Color) -> [Position; 8] {
        [Some(Piece::new(Pawn, c)); 8]
    };
    Board {
        state: [
            officiers(White),
            pawns(White),
            [None; 8],
            [None; 8],
            [None; 8],
            [None; 8],
            pawns(Black),
            officiers(Black),
        ],
    }
}

#[derive(Debug)]
pub enum MoveError {
    SameSquare,
    NotAPiece,
    FriendlyFire,
    WrongPlayer,
    IllegalMove,
    Pinned,
    UnresolvedCheck,
}

fn possible(game: &ChessState, m: &Move) -> Result<(), MoveError> {
    use MoveError::*;
    if m.from == m.to {
        return Err(SameSquare);
    }
    let piece = game.board[m.from].ok_or(NotAPiece)?;

    if piece.color() != game.player {
        return Err(WrongPlayer);
    }

    let mut valid = false;
    for possible in piece.moves(game, m.from) {
        if possible.to == m.to {
            valid = true;
        }
    }
    // if piece.typ() == Type::Pawn { valid = true };
    if !valid {
        return Err(MoveError::IllegalMove);
    }

    if let Some(taken) = game.board[m.to] {
        if taken.color() == piece.color() {
            return Err(FriendlyFire);
        }
    }

    Ok(())
}

fn is_friendly_fire(game: &ChessState, m: &Move) -> Result<(), MoveError> {
    let piece = game.board[m.from].ok_or(MoveError::NotAPiece)?;
    if let Some(taken) = game.board[m.to] {
        if taken.color() == piece.color() {
            return Err(MoveError::FriendlyFire);
        }
    }

    Ok(())
}

impl ChessState {
    pub fn new() -> Self {
        Self {
            board: initial_board(),
            player: Color::White,
            castling: Both {
                white: Castling {
                    king: true,
                    queen: true,
                },
                black: Castling {
                    king: true,
                    queen: true,
                },
            },
            en_passant: None,
            halfturns: 0,
        }
    }

    pub fn legal(&self, m: Move) -> Result<(), MoveError> {
        possible(self, &m)
    }

    pub fn apply(&mut self, m: Move) -> Result<(), MoveError> {
        self.legal(m)?;

        // Take Piece
        let piece = self.board[m.from].take().unwrap();

        //check if capture
        let capture = self.board[m.to].is_some();
        self.halfturns += 1;
        if capture {
            self.halfturns = 0;
        }

        //Set piece
        self.board[m.to] = Some(piece);

        // Promote
        if piece.typ() == Type::Pawn {
            if m.to.row() == 0 || m.to.row() == 7 {
                self.board[m.to] = Some(Piece::new(Type::Queen, piece.color()));
            }
        }

        // change player
        self.player = self.player.either(Color::Black, Color::White);

        Ok(())
    }
}

// ----------------------------------------------------------------
//          MOVE GENERATION
// ----------------------------------------------------------------

impl Board<Position> {
    fn target(&self, pos: BoardIdx, player: Color) -> Target {
        match self[pos] {
            Some(Piece { color, .. }) if color == player => Target::Occupied,
            Some(Piece { color, .. }) if color != player => Target::Capture,
            _ => Target::Free,
        }
    }
}

enum Target {
    Free,
    Occupied,
    Capture,
}

impl Target {
    fn unoccupied(&self) -> bool {
        match self {
            Target::Occupied => false,
            _ => true,
        }
    }

    fn is_capture(&self) -> bool {
        match self {
            Target::Capture => true,
            _ => false,
        }
    }

    fn is_free(&self) -> bool {
        match self {
            Target::Free => true,
            _ => false,
        }
    }
}

#[derive(Clone, Copy)]
enum Direction {
    North,
    East,
    West,
    South,
    Northwest,
    Northeast,
    Southwest,
    Southeast,
}

fn idx_move(from: BoardIdx, dir: Direction) -> Option<BoardIdx> {
    use Direction::*;
    let (d_row, d_col) = match dir {
        North => (1, 0),
        South => (-1, 0),
        East => (0, 1),
        West => (0, -1),
        Northeast => (1, 1),
        Northwest => (1, -1),
        Southwest => (-1, -1),
        Southeast => (-1, 1),
    };

    let (row, col) = (from.row() as i8, from.column() as i8);
    use std::convert::TryInto;
    (row + d_row, col + d_col).try_into().ok()
}

fn pawn_moves_with<'a>(
    game: &'a ChessState,
    from: BoardIdx,
    player: Color,
) -> impl Iterator<Item = Move> + 'a {
    use Direction::*;
    fn advance(game: &ChessState, from: BoardIdx, player: Color) -> Option<Move> {
        let next = idx_move(from, player.either(North, South))?;
        let no_capture = !game.board.target(next, player).is_capture();
        no_capture.then(|| Move { from, to: next })
    }

    fn double(game: &ChessState, from: BoardIdx, player: Color) -> Option<Move> {
        let start_row = player.either(1, 6);
        (start_row == from.row()).then(|| ())?;
        let dir = player.either(North, South);
        let advanced = idx_move(from, dir)?;
        game.board.target(advanced, player).is_free().then(|| ())?;
        let target = idx_move(advanced, dir)?;
        let no_capture = !game.board.target(target, player).is_capture();
        no_capture.then(|| ())?;
        Some(Move { from, to: target })
    }

    fn attack(game: &ChessState, from: BoardIdx, player: Color, dir: Direction) -> Option<Move> {
        let attack = idx_move(from, dir)?;
        let capture = game.board.target(attack, player).is_capture();
        capture.then(|| Move { from, to: attack })
    }

    let next = advance(game, from, player);
    let attack_east = attack(game, from, player, player.either(Northeast, Southeast));
    let attack_west = attack(game, from, player, player.either(Northwest, Southwest));
    let double = double(game, from, player);

    std::iter::empty()
        .chain(next.into_iter())
        .chain(attack_east.into_iter())
        .chain(attack_west.into_iter())
        .chain(double.into_iter())
}

fn pawn_moves<'a>(game: &'a ChessState, from: BoardIdx) -> impl Iterator<Item = Move> + 'a {
    game.board[from]
        .into_iter()
        .flat_map(move |c| pawn_moves_with(game, from, c.color()))
}

fn sliding_moves<'a>(
    game: &'a ChessState,
    from: BoardIdx,
    direction: Direction,
) -> impl Iterator<Item = Move> + 'a {
    let mut pos = Some(from);
    std::iter::from_fn(move || {
        let mut to = idx_move(pos.take()?, direction);
        match game.board.target(to?, game.board[from]?.color()) {
            Target::Free => {
                pos = to;
            }
            Target::Occupied => {
                to = None;
            }
            _ => {}
        }
        Some(Move { from, to: to? })
    })
}
fn king_moves(game: &ChessState, from: BoardIdx) -> impl Iterator<Item = Move> + '_ {
    let (row, col) = (from.row() as i8, from.column() as i8);
    [
        (1, 0),
        (0, 1),
        (0, -1),
        (-1, 0),
        (1, 1),
        (1, -1),
        (-1, 1),
        (-1, -1),
    ]
    .iter()
    .filter_map(move |(dr, dc)| {
        use std::convert::TryInto;
        let pos = (row + dr, col + dc).try_into().ok()?;
        let target = game.board.target(pos, game.board[from]?.color());
        if target.unoccupied() {
            Some(Move { from, to: pos })
        } else {
            None
        }
    })
}

fn knight_moves(game: &ChessState, from: BoardIdx) -> impl Iterator<Item = Move> + '_ {
    let (row, col) = (from.row() as i8, from.column() as i8);
    [
        (1, 2),
        (2, 1),
        (-1, 2),
        (2, -1),
        (1, -2),
        (-2, 1),
        (-1, -2),
        (-2, -1),
    ]
    .iter()
    .filter_map(move |(dr, dc)| {
        use std::convert::TryInto;
        let pos = (row + dr, col + dc).try_into().ok()?;
        let target = game.board.target(pos, game.board[from]?.color());
        if target.unoccupied() {
            Some(Move { from, to: pos })
        } else {
            None
        }
    })
}

fn queen_moves(game: &ChessState, from: BoardIdx) -> impl Iterator<Item = Move> + '_ {
    rook_moves(game, from).chain(bishop_moves(game, from))
}

fn bishop_moves(game: &ChessState, from: BoardIdx) -> impl Iterator<Item = Move> + '_ {
    std::iter::empty()
        .chain(sliding_moves(game, from, Direction::Northwest))
        .chain(sliding_moves(game, from, Direction::Northeast))
        .chain(sliding_moves(game, from, Direction::Southwest))
        .chain(sliding_moves(game, from, Direction::Southeast))
}

fn rook_moves(game: &ChessState, from: BoardIdx) -> impl Iterator<Item = Move> + '_ {
    std::iter::empty()
        .chain(sliding_moves(game, from, Direction::North))
        .chain(sliding_moves(game, from, Direction::South))
        .chain(sliding_moves(game, from, Direction::West))
        .chain(sliding_moves(game, from, Direction::East))
}

impl Piece {
    pub fn moves<'a>(
        &self,
        game: &'a ChessState,
        from: BoardIdx,
    ) -> impl Iterator<Item = Move> + 'a {
        enum TypeIter<A, B, C, D, E, F> {
            King(A),
            Queen(B),
            Bishop(C),
            Knight(D),
            Rook(E),
            Pawn(F),
        }

        impl<I, A1, B1, C1, D1, E1, F1> Iterator for TypeIter<A1, B1, C1, D1, E1, F1>
        where
            A1: Iterator<Item = I>,
            B1: Iterator<Item = I>,
            C1: Iterator<Item = I>,
            D1: Iterator<Item = I>,
            E1: Iterator<Item = I>,
            F1: Iterator<Item = I>,
        {
            type Item = I;
            fn next(&mut self) -> Option<I> {
                match self {
                    Self::King(x) => x.next(),
                    Self::Queen(x) => x.next(),
                    Self::Bishop(x) => x.next(),
                    Self::Knight(x) => x.next(),
                    Self::Rook(x) => x.next(),
                    Self::Pawn(x) => x.next(),
                }
            }
        }

        match self.typ() {
            Type::King => TypeIter::King(king_moves(game, from)),
            Type::Queen => TypeIter::Queen(queen_moves(game, from)),
            Type::Bishop => TypeIter::Bishop(bishop_moves(game, from)),
            Type::Knight => TypeIter::Knight(knight_moves(game, from)),
            Type::Rook => TypeIter::Rook(rook_moves(game, from)),
            Type::Pawn => TypeIter::Pawn(pawn_moves(game, from)),
        }
    }
}

pub fn all_possible_moves(game: &ChessState) -> impl Iterator<Item = Move> {
    let mut moves = Vec::new();
    for (idx, item) in game.board.each() {
        if let Some(piece) = item {
            if piece.color() == game.player {
                moves.extend(piece.moves(game, idx));
            }
        }
    }
    moves.into_iter()
}

// ----------------------------------------------------------------
//     MCTS GAME STATE IMPL
// ----------------------------------------------------------------

impl mcts::GameState for ChessState {
    type Move = Move;
    type Player = Color;
    type MoveList = Vec<Move>;

    fn current_player(&self) -> Self::Player {
        self.player
    }

    fn available_moves(&self) -> Self::MoveList {
        if self.halfturns > 75 {
            return vec![];
        }

        let mut kings = 0;
        for (_, square) in self.board.each() {
            if let Some(piece) = square {
                if piece.typ() == Type::King {
                    kings += 1;
                }
            }
        }

        if kings < 2 {
            return vec![];
        }

        all_possible_moves(&self)
            .filter(|&m| is_friendly_fire(self, &m).is_ok())
            .collect()
    }

    fn make_move(&mut self, mov: &Self::Move) {
        self.apply(mov.clone()).unwrap();
    }
}

impl mcts::transposition_table::TranspositionHash for ChessState {
    fn hash(&self) -> u64 {
        use std::collections::hash_map::DefaultHasher;
        use std::hash::{Hash, Hasher};
        let mut hasher = DefaultHasher::new();
        Hash::hash(self, &mut hasher);
        hasher.finish()
    }
}
