use mcts::{self, MCTSManager};

mod state;
use state::*;

fn count_material(board: &Board<Position>) -> i64 {
    let pieces = board.each().filter_map(|(_, &p)| p);
    fn valuation(p: &Type) -> i64 {
        match p {
            Type::King => 1,
            Type::Queen => 8,
            Type::Rook => 5,
            Type::Bishop => 4,
            Type::Knight => 3,
            Type::Pawn => 1,
        }
    }

    pieces
        .map(|p| valuation(&p.typ()) * p.color().either(1, -1))
        .sum()
}

/*
struct CountErr {
    depth: usize,
    game: ChessState,
    m: Move,
    err: MoveError,
}

fn make_moves(game: ChessState, depth: usize) -> Box<dyn Iterator<Item = ChessState>> {
    if depth == 0 {
        return Box::new(std::iter::once(game));
    }

    let moves = std::iter::once(game.clone()).chain(
        all_possible_moves(&game)
            .filter_map(move |m| {
                let mut new = game.clone();
                new.apply(m).ok()?;
                Some(new)
            })
            .flat_map(move |g| make_moves(g, depth - 1)),
    );

    Box::new(moves)

    /*
    all_possible_moves(&game).flat_map(|| {
        let mut new = game.clone();
        match new.apply(m) {
            Err(MoveError::FriendlyFire) => None,
            Err(err) => { return Err(CountErr { depth, game: new, m, err }) },
            _ => { count += count_moves(new, depth - 1)?; }
        }

    })*/
}

fn count_moves(game: ChessState, depth: usize) -> Result<usize, CountErr> {
    if depth == 0 {
        return Ok(1);
    }

    let mut count = 0;

    for m in all_possible_moves(&game) {
        let mut new = game.clone();
        match new.apply(m) {
            Err(MoveError::FriendlyFire) => {}
            Err(err) => {
                return Err(CountErr {
                    depth,
                    game: new,
                    m,
                    err,
                })
            }
            _ => {
                count += count_moves(new, depth - 1)?;
            }
        }
    }

    return Ok(count);
}
*/

#[derive(Default)]
struct ChessEvaluator;

#[derive(Default)]
struct ChessMCTS;

impl mcts::MCTS for ChessMCTS {
    type State = ChessState;
    type Eval = ChessEvaluator;
    type NodeData = ();
    type ExtraThreadData = ();
    type TreePolicy = mcts::tree_policy::UCTPolicy;
    type TranspositionTable = mcts::transposition_table::ApproxTable<Self>;
}

impl mcts::Evaluator<ChessMCTS> for ChessEvaluator {
    type StateEvaluation = i64;

    fn evaluate_new_state(
        &self,
        state: &ChessState,
        moves: &Vec<Move>,
        _handle: Option<mcts::SearchHandle<ChessMCTS>>,
    ) -> (Vec<()>, Self::StateEvaluation) {
        if moves.len() == 0 {
            let mut kings = 0;
            for (_, square) in state.board.each() {
                if let Some(piece) = square {
                    if piece.typ() == Type::King {
                        kings += 1;
                    }
                }
            }
            if kings >= 2 {
                return (vec![], 0);
            }
            let score = state.player.either(-100, 100);
            return (vec![], score);
        }
        (vec![(); moves.len()], count_material(&state.board))
    }

    fn evaluate_existing_state(
        &self,
        _state: &ChessState,
        existing_evaln: &Self::StateEvaluation,
        _handle: mcts::SearchHandle<ChessMCTS>,
    ) -> Self::StateEvaluation {
        *existing_evaln
    }

    fn interpret_evaluation_for_player(&self, e: &Self::StateEvaluation, player: &Color) -> i64 {
        match player {
            Color::White => *e,
            Color::Black => -*e,
        }
    }
}

extern crate sdl2;

use sdl2::image::LoadTexture;
use sdl2::keyboard::Keycode;
use sdl2::pixels;
use sdl2::{event::Event, gfx::primitives::DrawRenderer};
use std::time::Duration;

use sdl2::render;
use sdl2::video;

use sdl2::rect::Rect;
fn index_to_rect(idx: BoardIdx, size: u32) -> Rect {
    let y = idx.row() as i32;
    let x = idx.column() as i32;
    let sizei = size as i32;
    Rect::new(x * sizei, (7 - y) * sizei, size, size)
}

fn index_from_mouse(x: i32, y: i32, size: u32) -> Option<BoardIdx> {
    let sizei = size as i32;
    let ix = x / sizei;
    let iy = y / sizei;
    if 0 <= ix && ix <= 7 && 0 <= iy && iy <= 7 {
        Some(BoardIdx::new(7 - iy as u8, ix as u8))
    } else {
        None
    }
}

fn draw_mouse(canvas: &mut render::Canvas<video::Window>, x: i32, y: i32, size: u32) {
    if let Some(idx) = index_from_mouse(x, y, size) {
        let rect = index_to_rect(idx, size);
        canvas.set_draw_color(pixels::Color::RGB(100, 100, 0));
        canvas.fill_rect(rect).unwrap();
    }
}

fn draw_board(canvas: &mut render::Canvas<video::Window>) {
    let black = pixels::Color::RGB(181, 136, 99);
    let white = pixels::Color::RGB(240, 217, 187);

    for r in 0..64 {
        let idx = BoardIdx { idx: r };
        let square = index_to_rect(idx, 100);
        canvas.set_draw_color(idx.color().either(black, white));
        canvas.fill_rect(square).unwrap();
    }
}

fn piece_to_sprite(piece: Piece) -> Rect {
    use Type::*;
    let sprite_x = match piece.typ() {
        King => 0,
        Queen => 1,
        Bishop => 2,
        Knight => 3,
        Rook => 4,
        Pawn => 5,
    };
    let width: u32 = 2000 / 6;
    let sprite_y = piece.color().either(0, width as i32);
    Rect::new(sprite_x * width as i32, sprite_y, width, width)
}

fn draw_pieces(
    canvas: &mut render::Canvas<video::Window>,
    board: &Board<Position>,
    texture: &render::Texture,
) {
    for (idx, p) in board.each() {
        let square = index_to_rect(idx, 100);
        if let Some(piece) = *p {
            let sprite = piece_to_sprite(piece);
            canvas.copy(texture, sprite, square).unwrap();
        }
    }
}

// const PIECES_SMALL: &[u8] = include_bytes!("pieces.svg");
const PIECES: &[u8] = include_bytes!("sprites.png");

struct Settings {
    ai: Both<bool>,
    fps: u32,
    steps: Both<i64>,
    pv: usize,
    threads: usize,
    exploration_constant: Both<f64>,
}

fn main() {
    let mut game = ChessState::new();

    let settings = Settings {
        ai: Both::new(true, true),
        fps: 60,
        steps: Both::new(45, 45),
        pv: 0,
        threads: 4,
        exploration_constant: Both::new(1.5, 1.0),
    };
    run(&mut game, &settings);
}

fn run(game: &mut ChessState, settings: &Settings) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem
        .window("chess demo", 800, 800)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    let texture_creator = canvas.texture_creator();
    let texture = texture_creator.load_texture_bytes(PIECES).unwrap();

    canvas.set_draw_color(pixels::Color::RGB(194, 97, 31));
    canvas.clear();
    canvas.present();

    let mut selection: Option<BoardIdx> = None;
    let mut mouse_x = 0;
    let mut mouse_y = 0;

    let mut event_pump = sdl_context.event_pump().unwrap();

    let mut search = mcts::MCTSManager::new(
        game.clone(),
        ChessMCTS,
        ChessEvaluator,
        mcts::tree_policy::UCTPolicy::new(*settings.exploration_constant.choose(game.player)),
        mcts::transposition_table::ApproxTable::new(4096),
    );

    let mut steps = 0;

    let mut mov = None;

    'running: loop {
        canvas.clear();
        draw_board(&mut canvas);

        if let Some(idx) = selection {
            if let Some(_piece) = game.board[idx] {
                for x in 0..64 {
                    let target = BoardIdx { idx: x };
                    let m = Move {
                        from: idx,
                        to: target,
                    };

                    if game.legal(m).is_ok() {
                        let rect = index_to_rect(target, 100);
                        let white = pixels::Color::RGB(214, 59, 74);
                        let black = pixels::Color::RGB(177, 38, 49);
                        let color = target.color().either(white, black);
                        canvas.set_draw_color(color);
                        canvas.fill_rect(rect).unwrap();
                    }
                }
            }
        }

        for info in search.principal_variation_info(settings.pv) {
            let color = pixels::Color::RGB(55, 77, 240);
            let rect = index_to_rect(info.get_move().from, 100);
            let target = index_to_rect(info.get_move().to, 100);
            canvas.set_draw_color(color);
            canvas
                .filled_circle(
                    target.center().x() as i16,
                    target.center().y() as i16,
                    30,
                    color,
                )
                .unwrap();
            canvas
                .thick_line(
                    rect.center().x() as i16,
                    rect.center().y() as i16,
                    target.center().x() as i16,
                    target.center().y() as i16,
                    30,
                    color,
                )
                .unwrap();
        }

        draw_pieces(&mut canvas, &game.board, &texture);

        if let Some(idx) = selection {
            if let Some(piece) = game.board[idx] {
                let rect = index_to_rect(idx, 100);
                canvas.set_draw_color(pixels::Color::RGB(194, 97, 31));
                canvas.fill_rect(rect).unwrap();

                let sprite = piece_to_sprite(piece);
                let floating = Rect::new(mouse_x - 50, mouse_y - 50, 100, 100);
                canvas.copy(&texture, sprite, floating).unwrap();
            }
        }

        canvas.present();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::MouseMotion { x, y, .. } => {
                    mouse_x = x;
                    mouse_y = y;
                }
                Event::MouseButtonDown { x, y, .. } => {
                    if let Some(idx) = index_from_mouse(x, y, 100) {
                        selection = Some(idx);
                    }
                }
                Event::MouseButtonUp { x, y, .. } => {
                    if let Some(idx) = index_from_mouse(x, y, 100) {
                        if let Some(pos) = selection {
                            mov = Some(Move { from: pos, to: idx });
                        }
                        selection = None;
                    }
                }
                _ => {}
            }
        }

        let frametime = Duration::new(0, 1_000_000_000u32 / settings.fps);
        
        if steps <= *settings.steps.choose(game.player) {
            search.playout_parallel_for(frametime, settings.threads);
            steps += 1;
        }
        
        if steps >= *settings.steps.choose(game.player) && *settings.ai.choose(game.player) {
            println!("search stats:");
            search.tree().debug_moves();
            mov = search.best_move();

            if mov.is_none() {
                println!("no best move");
                *game = ChessState::new();
                search = mcts::MCTSManager::new(
                    game.clone(),
                    ChessMCTS,
                    ChessEvaluator,
                    mcts::tree_policy::UCTPolicy::new(*settings.exploration_constant.choose(game.player)),
                    mcts::transposition_table::ApproxTable::new(4096),
                );
                steps = 0;
            }
        }

        if let Some(m) = mov.take() {
            if let Err(e) = game.apply(m) {
                println!("Error: {:?}", e);
            } else {
                steps = 0;
                search = mcts::MCTSManager::new(
                    game.clone(),
                    ChessMCTS,
                    ChessEvaluator,
                    mcts::tree_policy::UCTPolicy::new(*settings.exploration_constant.choose(game.player)),
                    mcts::transposition_table::ApproxTable::new(4096),
                );
            }
        }
        //::std::thread::sleep(frametime);
    }
}
